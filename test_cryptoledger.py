import unittest
from cryptoledger import CryptoLedger
import os


class CryptoLedgerTests(unittest.TestCase):
    '''
    Test cases
    '''
    def test_init_keys(self):
        '''
        Test Init Keys
        '''

        CryptoLedger.init_keys(
            admin_private_key='test_cryptoledger_admin.key',
            admin_public_key='test_cryptoledger_admin.pub',
            logger_private_key='test_cryptoledger_logger.key',
            logger_public_key='test_cryptoledger_logger.pub')

        self.assertTrue(os.path.exists('test_cryptoledger_admin.key'))
        self.assertTrue(os.path.exists('test_cryptoledger_admin.pub'))
        self.assertTrue(os.path.exists('test_cryptoledger_logger.key'))
        self.assertTrue(os.path.exists('test_cryptoledger_logger.pub'))

        os.remove('test_cryptoledger_admin.key')
        os.remove('test_cryptoledger_admin.pub')
        os.remove('test_cryptoledger_logger.key')
        os.remove('test_cryptoledger_logger.pub')

    def test_log(self):
        '''
        Test Log
        '''
        CryptoLedger.init_keys(
            admin_private_key='test_cryptoledger_admin.key',
            admin_public_key='test_cryptoledger_admin.pub',
            logger_private_key='test_cryptoledger_logger.key',
            logger_public_key='test_cryptoledger_logger.pub')

        self.assertTrue(os.path.exists('test_cryptoledger_admin.key'))
        self.assertTrue(os.path.exists('test_cryptoledger_admin.pub'))
        self.assertTrue(os.path.exists('test_cryptoledger_logger.key'))
        self.assertTrue(os.path.exists('test_cryptoledger_logger.pub'))

        l_log = CryptoLedger(
            logfile='test.log',
            admin_public_key='test_cryptoledger_admin.pub',
            logger_private_key='test_cryptoledger_logger.key',
            logger_public_key='test_cryptoledger_logger.pub')
        l_log.log('test')
        for i in range(10):
            l_log.log(f'bar {i}')
        self.assertTrue(l_log.verify_log('test_cryptoledger_admin.key'))

        os.remove('test_cryptoledger_admin.key')
        os.remove('test_cryptoledger_admin.pub')
        os.remove('test_cryptoledger_logger.key')
        os.remove('test_cryptoledger_logger.pub')
        os.remove('test.log')


if __name__ == '__main__':
    unittest.main()
