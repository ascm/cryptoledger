import argparse
import json
import nacl.utils
from nacl.bindings.utils import sodium_memcmp
from nacl.encoding import Base64Encoder
from nacl.hash import sha256
from nacl.public import Box
from nacl.public import PrivateKey
from nacl.public import PublicKey
import os
import time


class CryptoLedger():
    def __init__(
            self,
            logfile,
            time_format='%Y-%m-%d %H-%M-%S',
            admin_public_key='cryptoledger_admin.pub',
            logger_private_key='cryptoledger_logger.key',
            logger_public_key='cryptoledger_logger.pub'):
        self._logfile = logfile
        self._time_format = time_format
        self.__hash = None
        if not (os.path.exists(admin_public_key) and
                os.path.exists(logger_private_key) and
                os.path.exists(logger_public_key)):
            raise FileExistsError(f'Key file(s) not found!')

        with open(admin_public_key, 'rb') as f_adminpub:
            self.__pk_admin = PublicKey(f_adminpub.read(), encoder=Base64Encoder)
        with open(logger_public_key, 'rb') as f_loggerpub:
            self.__pk_logger = PublicKey(f_loggerpub.read(), encoder=Base64Encoder)
        with open(logger_private_key, 'rb') as f_loggerkey:
            self.__sk_logger = PrivateKey(f_loggerkey.read(), encoder=Base64Encoder)
        if os.path.exists(logfile):
            with open(self._logfile) as f_log:
                l_lines = f_log.readlines()
                if not l_lines:
                    print(f'Provided log file `{self._logfile}` is empty! Initialising with new nonce.')
                    self.init_logfile()
                    return
                self.__hash = json.loads(l_lines[-1]).get(
                    'hash',
                    json.loads(l_lines[-1]).get('init nonce')).encode()
            if not self.__hash:
                raise RuntimeError(f'Provided log file `{self._logfile}`'
                                   'does not contain an initial nonce or hash.'
                                   'Run `init_logfile()` first or privde a valid log file.')
        else:
            self.init_logfile()

    def init_logfile(self):
        # add initial encrypted random value
        l_init_nonce = Base64Encoder.encode(nacl.utils.random())
        self.__hash = sha256(l_init_nonce)
        with open(self._logfile, 'w') as f_log:
            f_log.write(
                json.dumps(
                    {
                        'init nonce': Base64Encoder.encode(
                            Box(self.__sk_logger, self.__pk_admin).encrypt(l_init_nonce)).decode(),
                        'hash': self.__hash.decode()
                    }) + '\n')
        del l_init_nonce

    @staticmethod
    def init_keys(
            admin_private_key='cryptoledger_admin.key',
            admin_public_key='cryptoledger_admin.pub',
            logger_private_key='cryptoledger_logger.key',
            logger_public_key='cryptoledger_logger.pub'):
        if os.path.exists(admin_public_key) \
                or os.path.exists(logger_private_key) \
                or os.path.exists(logger_public_key):
            raise FileExistsError(f'Refusing to overwrite existing key file(s)!')
        sk_logger = PrivateKey.generate()
        pk_logger = sk_logger.public_key
        sk_admin = PrivateKey.generate()
        pk_admin = sk_admin.public_key
        with open(admin_private_key, 'wb') as f_adminkey:
            f_adminkey.write(sk_admin.encode(encoder=Base64Encoder))
        with open(admin_public_key, 'wb') as f_adminpub:
            f_adminpub.write(sk_admin.public_key.encode(encoder=Base64Encoder))
        with open(logger_private_key, 'wb') as f_loggerkey:
            f_loggerkey.write(sk_logger.encode(encoder=Base64Encoder))
        with open(logger_public_key, 'wb') as f_loggerpub:
            f_loggerpub.write(
                sk_logger.public_key.encode(encoder=Base64Encoder))
        del sk_logger
        del sk_admin

    def log(self, message: str):
        '''
        :param message: append message to ledger
        '''
        l_time = str(time.time())
        self.__hash = sha256(l_time.encode() + message.encode() + self.__hash)

        with open(self._logfile, 'a') as f_log:
            f_log.write(
                json.dumps(
                    {
                        'time': l_time,
                        'message': message,
                        'hash': self.__hash.decode()
                    }) + '\n')

    def verify_log(self, admin_key_file):
        '''
        :param admin_key_file: private key file name
        '''
        with open(admin_key_file, 'rb') as f_adminkey, open(self._logfile) as f_log:
            l_sk_admin = PrivateKey(f_adminkey.read(), encoder=Base64Encoder)
            l_log_lines = f_log.readlines()
            l_hash = sha256(
                Box(
                    l_sk_admin,
                    self.__pk_logger).decrypt(
                        json.loads(l_log_lines[0]).get('init nonce'), encoder=Base64Encoder))
            del l_sk_admin

            if sodium_memcmp(json.loads(l_log_lines[0]).get('hash').encode(), l_hash):
                print('nonce verified!')
            else:
                print('nonce hashes do not match!')
                return False

            l_first_failed = None
            for i_log_line in l_log_lines[1:]:
                l_log_entry = json.loads(i_log_line)
                l_hash = sha256(
                    l_log_entry.get('time').encode() +
                    l_log_entry.get('message').encode() +
                    l_hash)
                if sodium_memcmp(l_hash, l_log_entry.get('hash').encode()):
                    print('.', end='')
                else:
                    print('f', end='')
                    l_first_failed = l_log_entry if not l_first_failed else l_first_failed
            print()
            if l_first_failed:
                print(f'"{l_log_entry}" (and all following log entries) fails to verify!')
                return False

            print(' all log entries verified!')
            return True


def main(args):
    if args.initkeys:
        CryptoLedger.init_keys()
    if args.verify:
        l_log = CryptoLedger(logfile=args.logfile)
        l_log.verify_log(args.adminkey)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        prog='cryptoledger.py',
        description='CryptoLedger',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument(
        '--verify',
        dest='verify',
        action='store_true',
        default=False,
        help='Verify log file.')
    parser.add_argument(
        '--logfile',
        dest='logfile',
        metavar='LOGFILE',
        type=str,
        default='clog.log',
        help='Verify log file.')
    parser.add_argument(
        '--admin-private-key',
        dest='adminkey',
        metavar='KEYFILE',
        type=str,
        default='cryptoledger_admin.key',
        help='Admin private key.')
    parser.add_argument(
        '--init-keys',
        dest='initkeys',
        action='store_true',
        default=False,
        help='Generate new admin and logger keys.')
    args = parser.parse_args()
    main(args)
